import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class FastapiService {

  constructor(private http: HttpClient) { }

  get_predict(invoice) {
    let headers = new HttpHeaders({
      'Content-Type':  'application/json',
    })
    let list = [invoice]
    return this.http.post('http://127.0.0.1:4000/api/v1/predict/', list, {headers: headers});
  }
}
