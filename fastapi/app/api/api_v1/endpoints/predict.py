from fastapi import APIRouter
from app.scikit_model.scoring_ml_module import ScoreModel
from typing import List

from app.models.invoice import Invoice

router = APIRouter()


@router.get("/")
def description():
    """
    Retrieve items.
    """
    return {'Description': 'Use POST method for predicting Claim probability'}


@router.post("/", response_model=List[Invoice])
def predict(invoices: List[Invoice]):
    """
    Create new item.
    """

    model = ScoreModel()
    predicted = model.predict(invoices)
    for i, invoice in enumerate(invoices):
        invoice.probability_0 = predicted[i][0]
        invoice.probability_delay = predicted[i][1]
        invoice.probability_cargo = predicted[i][2]
    return invoices
