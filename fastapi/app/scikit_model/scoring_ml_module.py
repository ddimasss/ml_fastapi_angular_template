from datetime import datetime
from app.models.invoice import Invoice
import joblib


class ScoreModel:
    """ Scoring class
        1. Init only once
        2. Use predict method
    """

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ScoreModel, cls).__new__(cls)
        return cls.instance

    filename = 'app/scikit_model/model_02_09_2020.scikit.sav'
    joblib_model = joblib.load(filename)
    joblib_model.b_jobs = -1
    filename_dicts = 'app/scikit_model/dicts_02_09_2000.sav'
    dicts = joblib.load(filename_dicts)
    dict_weeks = dicts['dict_weeks']
    dict_departure = dicts['dict_departure']
    dict_arrival = dicts['dict_arrival']
    dict_cargokind = dicts['dict_cargokind']
    dict_servicekind = dicts['dict_servicekind']

    def __init__(self):
        pass

    def predict(self, invoices: list):
        x_test = self.prepare_data(invoices)
        y_predicted = self.joblib_model.predict_proba(x_test)
        return y_predicted

    def prepare_data(self, invoices):
        x_test = []
        # week_ratio, departure_kladr_ratio, arrival_kladr_ratio, cargokind_ratio, service_kind_ratio, amount, declaredvalueinkops, freightinkops, netvolume, netweight
        for invoice in invoices:
            week = invoice.date.isocalendar()[1]
            list_ = self.dict_weeks[
                week].copy()
            if invoice.sender_id in self.dict_departure:
                list_.extend(self.dict_departure[invoice.sender_id].copy())
            else:
                list_.extend(self.dict_departure[''].copy())
            if invoice.receiver_id in self.dict_arrival:
                list_.extend(self.dict_arrival[invoice.receiver_id].copy())
            else:
                list_.extend(self.dict_arrival[''].copy())

            if invoice.cargo_kind in self.dict_cargokind:
                list_.extend(self.dict_cargokind[invoice.cargo_kind].copy())
            else:
                list_.extend(self.dict_cargokind[''].copy())
            if invoice.service_kind in self.dict_servicekind:
                list_.extend(self.dict_servicekind[invoice.service_kind].copy())
            else:
                list_.extend(self.dict_servicekind[''].copy())
            # amount, declaredvalueinkops, freightinkops, netvolume, netweight
            tmp = [invoice.amount, invoice.declared_value, invoice.freight, invoice.volume, invoice.weight]
            list_.extend(tmp)
            x_test.append(list_)
        return x_test