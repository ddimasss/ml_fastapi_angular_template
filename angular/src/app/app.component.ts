import { Component } from '@angular/core';
import { FastapiService } from './service/fastapi.service';


export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

export interface Invoice {
  date?: string; 
  sender_id?: string;
  receiver_id?: string;
  freight?: number;    // Миллион в копейках
  declared_value?: number; // Миллион в копейках
  cargo_kind?: string;
  service_kind?: string;
  weight?: number;
  volume?: number;
  amount?: number;
  probability_0?: number;
  probability_delay?: number;
  probability_cargo?: number;
}

const INVOICES: Invoice[] = [
  {date: '2020-08-01T12:27', sender_id: '1600000100000000000000000', receiver_id: '7700000000000000000000000', freight: 550000, declared_value: 8915000, cargo_kind: 'Мебель', service_kind: 'Почасовая оплата', weight: 1.30999994, volume: 1, amount: 195},
  {date: '2020-08-10T12:27', sender_id: '7700000000000000000000000', receiver_id: '7700000000000000000000000', freight: 29000, declared_value: 0, cargo_kind: 'Запчасти', service_kind: 'Авто', weight: 0.3, volume: 2, amount: 0.1},
  {date: '2020-08-12T12:27', sender_id: '1100000500000000000000000', receiver_id: '4600000100000000000000000', freight: 340000, declared_value: 300000, cargo_kind: 'Оборудование', service_kind: 'Авто', weight: 0.1, volume: 36, amount: 1},
  {date: '2020-08-13T12:27', sender_id: '2500000300000000000000000', receiver_id: '6800400100000000000000000', freight: 325800, declared_value: 200000, cargo_kind: 'Оргтехника и ПК', service_kind: 'Авто', weight: 0.28, volume: 39, amount: 3},
  {date: '2020-08-14T12:27', sender_id: '5004100000000000000000000', receiver_id: '5000003100000000000000000', freight: 69000, declared_value: 200000, cargo_kind: 'Документы', service_kind: 'Авто', weight: 0.1, volume: 1, amount: 1},
  {date: '2020-08-15T12:27', sender_id: '7400000100000000000000000', receiver_id: '4700500100000000000000000', freight: 86600, declared_value: 21742000, cargo_kind: 'Электродвигатели', service_kind: 'Авто', weight: 0.12, volume: 1, amount: 2},
  {date: '2020-08-16T12:27', sender_id: '6500000100000000000000000', receiver_id: '3800000300000000000000000', freight: 1833200, declared_value: 750000, cargo_kind: 'Запчасти', service_kind: 'Авто', weight: 0.69999999, volume: 290, amount: 1},
  {date: '2020-08-17T12:27', sender_id: '', receiver_id: '', freight: 556202, declared_value: 19195607, cargo_kind: 'Водоэмульсионная краска', service_kind: 'авто+жд', weight: 0, volume: 0, amount: 0},
  {date: '2020-08-19T12:27', sender_id: '2500000100000000000000000', receiver_id: '2400000100000000000000000', freight: 82000, declared_value: 900000, cargo_kind: 'Автозапчасти', service_kind: 'Авто', weight: 44, volume: 0.1, amount: 2},
  {date: '2020-08-05T12:27', sender_id: '7700000000000000000000000', receiver_id: '3800000300000000000000000', freight: 391000, declared_value: 0, cargo_kind: 'Лента', service_kind: 'Авто', weight: 0.23999999, volume: 110, amount: 11}
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private service: FastapiService) { }

  title = 'Python meetup #2';
  displayedColumns: string[] = ['date', 'freight', 'declared_value', 'cargo_kind', 'weight', 'volume', 'probability', 'actions'];
  dataSource = INVOICES;

  getProb(el: Invoice) {
    this.service.get_predict(el).subscribe(data => {
      el.probability_0 = Math.round(data[0]['probability_0'] * 100); 
      el.probability_delay =  Math.round(data[0]['probability_delay'] * 100); 
      el.probability_cargo = Math.round(data[0]['probability_cargo'] * 100);
      console.log(data[0]);
    });
  }
}
