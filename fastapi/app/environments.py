from pydantic import BaseSettings


class Settings(BaseSettings):
    APP_NAME: str = 'your_name'
    URL_PREFIX_V1: str = '/api/v1'
    ENV: str = 'prod'

    class Config:
        def __init__(self):
            pass

        env_prefix = 'lab_'


settings = Settings()
