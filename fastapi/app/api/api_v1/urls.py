from fastapi import Depends, APIRouter

from app.api.api_v1.endpoints import predict

api_v1_router = APIRouter()
api_v1_router.include_router(predict.router, prefix="/predict", tags=["predict"])
