from fastapi import FastAPI, Depends
import uvicorn
from starlette.middleware.cors import CORSMiddleware
from app.environments import settings
from app.api.api_v1.urls import api_v1_router
from app.scikit_model.scoring_ml_module import ScoreModel

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=["*"],
    allow_credentials=True,
    allow_headers=["*"]
)

@app.on_event("startup")
async def startup_event():
    """ Loading ML model """

    global model
    print('Start loading ML model')
    ScoreModel()
    print('Done')


app.include_router(api_v1_router, prefix=settings.URL_PREFIX_V1)


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=40000, log_level="info", reload=False)
