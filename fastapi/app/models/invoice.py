from datetime import datetime
from typing import Optional
from pydantic import BaseModel

class Invoice(BaseModel):
    date: Optional[datetime] = datetime.now()
    sender_id: Optional[str] = '3300000100000000000000000'
    receiver_id: Optional[str] = '5000002400000000000000000'
    freight: Optional[int] = 100000000    # Миллион в копейках
    declared_value: Optional[int] = 100000000 # Миллион в копейках
    cargo_kind: Optional[str] = ''
    service_kind: Optional[str] = ''
    weight: Optional[float] = 10000.0
    volume: Optional[float] = 10000.0
    amount: Optional[int] = 50
    probability_0: Optional[float] = None
    probability_delay: Optional[float] = None
    probability_cargo: Optional[float] = None

